from django.contrib import admin

# Register your models here.
from android_backend.models import Sprint, Task


class SprintAdmin(admin.ModelAdmin):
    class Meta:
        model = Sprint


class TaskAdmin(admin.ModelAdmin):
    class Meta:
        model = Task

admin.site.register(Sprint, SprintAdmin)
admin.site.register(Task, SprintAdmin)
