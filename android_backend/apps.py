from django.apps import AppConfig


class AndroidBackendConfig(AppConfig):
    name = 'android_backend'
