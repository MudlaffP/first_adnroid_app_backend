from django.db import models

# Create your models here.


class Sprint(models.Model):
    name = models.CharField(max_length=40, unique=True)
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Task(models.Model):
    TASK_STATUSES = (
        ('C', 'Created'),
        ('TD', 'To do'),
        ('T', 'Test'),
        ('D', 'Done')
    )
    name = models.CharField(max_length=40)
    status = models.CharField(max_length=5, choices=TASK_STATUSES)
    description = models.TextField(null=True, blank=True, default='')
    sprint = models.ForeignKey(Sprint)
