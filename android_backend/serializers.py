from rest_framework import serializers

from android_backend.models import Sprint, Task


class SprintSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sprint
        fields = ('id', 'name')


class SprintBaseSerializer(serializers.BaseSerializer):
    def to_representation(self, instance):
        return {
            'id': instance.id,
            'name': instance.name
        }


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
