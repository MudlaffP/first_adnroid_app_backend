import logging

from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets, status
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response

from android_backend.models import Sprint, Task
from android_backend.serializers import SprintSerializer, TaskSerializer, SprintBaseSerializer

logger = logging.getLogger('main_logger')


class SprintVS(viewsets.ModelViewSet):
    model = Sprint
    queryset = Sprint.objects.all()
    serializer_class = SprintBaseSerializer

    @list_route(methods=['POST'])
    def get_active_sprint(self, request, *args, **kwargs):
        try:
            active_sprint = Sprint.objects.get(is_active=True)
            logger.debug('Active sprint: {}'.format(active_sprint))
            active_sprint_serialized = SprintBaseSerializer(active_sprint)

            return Response(
                active_sprint_serialized.data,
                status=status.HTTP_200_OK
            )
        except Sprint.DoesNotExist:
            return Response(
                'no active sprint',
                status=status.HTTP_202_ACCEPTED
            )


class TaskVS(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

    @list_route(methods=['POST'])
    def get_active_sprint_group_tasks(self, request, *args, **kwargs):
        try:
            sprint = Sprint.objects.get(id=int(request.data.get('sprint_id')))
        except Sprint.DoesNotExist:
            return Response(
                {'message': 'No active sprint'},
                status=status.HTTP_404_NOT_FOUND
            )

        task_group = request.data.get('task_group')

        if task_group.lower() == 'created':
            tasks = Task.objects.filter(sprint__id=sprint.id, status='C')
        elif task_group.lower() == 'to do':
            tasks = Task.objects.filter(sprint__id=sprint.id, status='TD')
        elif task_group.lower() == 'test':
            tasks = Task.objects.filter(sprint__id=sprint.id, status='T')
        elif task_group.lower() == 'done':
            tasks = Task.objects.filter(sprint__id=sprint.id, status='D')
        else:
            return Response(
                {'message': 'Incorect sprint group'},
                status=status.HTTP_404_NOT_FOUND
            )

        serializer = TaskSerializer(tasks, many=True)

        return Response(
            {'tasks': serializer.data},
            status=status.HTTP_200_OK
        )
